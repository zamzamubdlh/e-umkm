<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Pimpinan',
            'email' => 'pimpinan@gmail.com',
            'password' => bcrypt('12345678'),
            'password_verifikasi' => bcrypt('12345678'),
            'role' => 'Pimpinan',
        ]);

        User::create([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('12345678'),
            'password_verifikasi' => bcrypt('12345678'),
            'role' => 'Admin',
        ]);

        User::create([
            'name' => 'Ds Kel12',
            'email' => 'dskel12@gmail.com',
            'password' => bcrypt('12345678'),
            'password_verifikasi' => bcrypt('12345678'),
            'role' => 'Customer',
        ]);
    }
}
