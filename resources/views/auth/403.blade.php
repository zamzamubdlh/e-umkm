@extends('layouts.master')
@section('title')
    Tidak diijinkan!
@endsection
@section('judul')
    Peringatan
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col text-center align-self-center pt-4">
                <h1> <i class="fas fa-exclamation-triangle text-danger fa-3x"></i></h1>
                <br>
                <h4><strong>Anda tidak memiliki akses ke halaman ini!</strong></h4>
                <p class="text-muted">Informasi lebih lanjut silahkan hubungi Admin anda.</p>
            </div>
        </div>
    </div>
@endsection
