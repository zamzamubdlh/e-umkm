@extends('layouts.master')
@section('title')
    Edit Data Produk
@endsection
@section('active')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Produk</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ url('home')}}">Home</a></li>
                    <li class="breadcrumb-item active">Data Produk</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card card-outline card-primary">
                <div class="card-header">
                    Edit Data Produk
                </div>
                <div class="card-body">
                    <form action="{{ route('produk.update', $produk->id) }}" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_method" value="PUT">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="">Nama Produk</label>
                                    <input type="text" name="nama_produk" value="{{ $produk->nama_produk }}" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="">Berat</label>
                                    <input type="text" name="berat" value="{{ $produk->berat }}" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Kategori</label>
                                    <select name="kategori_id" id="exampleFormControlSelect1" class="form-control">
                                        @foreach ($kategori as $data)
                                            <option value="{{ $data->id }}" @if ($data->nama_kategori == $produk->kategori->nama_kategori) selected @endif>
                                                {{ $data->nama_kategori }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Harga</label>
                                    <input type="text" name="harga" value="{{ $produk->harga }}" class="form-control">
                                </div>
                                <div class="form-group">
                                    <img src="{{ asset('storage/' .$produk['gambar']) }}" width="50%" alt="" class="img-thumbnail">
                                </div>
                                <div class="form-group">
                                    <label for="gambar">Dokumen Standar</label>
                                    <div class="custom-file">
                                        <input type="file" name="gambar" id="customFile" class="custom-file-input">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <a href="{{ url()->previous() }}" class="btn btn-outline-secondary btn-block"><i class="fa fa-arrow-alt-circle-left"></i> Kembali</a>
                            <button type="submit" class="btn btn-outline-primary btn-block"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
<!-- bs-custom-file-input -->
<script src="{{ asset('assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
<!-- Page specific script -->
<script>
    $(function () {
    bsCustomFileInput.init();
    });
</script>
@endpush