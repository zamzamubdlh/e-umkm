<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use Illuminate\Http\Request;

class FotoProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $foto_produk = Produk::all();
        return view('admin.foto_produk.index', compact('foto_produk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FotoProduk  $fotoProduk
     * @return \Illuminate\Http\Response
     */
    public function show(FotoProduk $fotoProduk)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FotoProduk  $fotoProduk
     * @return \Illuminate\Http\Response
     */
    public function edit(FotoProduk $fotoProduk)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FotoProduk  $fotoProduk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FotoProduk $fotoProduk)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FotoProduk  $fotoProduk
     * @return \Illuminate\Http\Response
     */
    public function destroy(FotoProduk $fotoProduk)
    {
        //
    }
}
