<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk = Produk::with('kategori')->get();
        return view('admin.produk.index', compact('produk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::all();
        return view('admin.produk.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_produk' => 'required',
            'berat' => 'required',
            'harga' => 'required',
            'gambar' => 'required'
        ], [
            'nama_produk.required' => 'Nama Produk tidak boleh kosong',
            'berat.required' => 'Berat tidak boleh kosong',
            'harga.required' => 'Harga tidak boleh kosong',
            'gambar.required' => 'Gambar tidak boleh kosong'
        ]);

        if ($request->file('gambar')) {
            $gambar = $request->file('gambar')->store('customFile', 'public');
        } else {
            $gambar = null;
        }

        Produk::insert([
            'nama_produk' => $request->get('nama_produk'),
            'berat' => $request->get('berat'),
            'kategori_id' => $request->get('kategori_id'),
            'harga' => $request->get('harga'),
            'gambar' => $gambar
        ]);
        return redirect()->route('produk.index')->with('message', 'Produk berhasil dibuat!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produk = Produk::findOrFail($id);
        $kategori = Kategori::all();
        return view('admin.produk.edit', compact('produk', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->file('gambar')) {
            $gambar = $request->file('gambar')->store('customFile', 'public');
            $data = Produk::findOrFail($id);
            if ($data->gambar) {
                Storage::delete('public/' . $data->gambar);
                $data->gambar = $gambar;
            } else {
                $data->gambar = $gambar;
            }
            $data->save();
        }

        $produk = Produk::findOrFail($id);
        $produk->nama_produk = $request->nama_produk;
        $produk->berat = $request->berat;
        $produk->kategori_id = $request->kategori_id;
        $produk->harga = $request->harga;
        $produk->save();
        return redirect()->route('produk.index')->with('message', 'Produk berhasil diedit!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk = Produk::findOrFail($id)->delete();
        return redirect()->route('produk.index')->with('message', 'Produk berhasil dihapus!');
    }
}
