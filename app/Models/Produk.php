<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    use HasFactory;
    protected $fillable = ['nama_produk', 'berat', 'kategori_id', 'harga', 'gambar'];
    public $timestamps = true;

    public function kategori(){
        return $this->belongsTo('App\Models\Kategori', 'kategori_id');
    }
}
